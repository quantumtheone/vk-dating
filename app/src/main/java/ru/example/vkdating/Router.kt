package ru.example.vkdating

import androidx.fragment.app.Fragment
import ru.example.vkdating.core.Screen

fun Fragment.addScreen(screen: Screen) {
    requireFragmentManager().beginTransaction()
            .apply {
                requireFragmentManager().findFragmentById(R.id.vgContainer)
                        ?.let { hide(it) }
            }
            .addToBackStack(null)
            .add(R.id.vgContainer, screen.fragment())
            .commit()
}

fun Fragment.replaceScreen(screen: Screen) {
    val fragmentManager = requireFragmentManager()
    while (fragmentManager.backStackEntryCount > 0) {
        fragmentManager.popBackStackImmediate()
    }
    fragmentManager.beginTransaction()
            .replace(R.id.vgContainer, screen.fragment())
            .commit()
}

fun Fragment.back() {
    val fragmentManager = requireFragmentManager()
    if (fragmentManager.backStackEntryCount > 0)
        fragmentManager.popBackStackImmediate()
    else
        requireActivity().onBackPressed()
}
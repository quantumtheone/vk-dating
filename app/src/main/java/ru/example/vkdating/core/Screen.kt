package ru.example.vkdating.core

import androidx.fragment.app.Fragment

abstract class Screen {
    abstract fun fragment(): Fragment
}
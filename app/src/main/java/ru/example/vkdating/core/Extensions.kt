package ru.example.vkdating.core

import androidx.fragment.app.Fragment
import ru.example.vkdating.App
import ru.example.vkdating.data.AuthPreferences
import ru.example.vkdating.data.UserPreferences
import ru.example.vkdating.data.network.NetworkManager

val Fragment.authPreferences: AuthPreferences
    get() = (requireContext().applicationContext as App).authPreference

val Fragment.userPreferences: UserPreferences
    get() = (requireContext().applicationContext as App).userPreference

val Fragment.networkManager: NetworkManager
    get() = (requireContext().applicationContext as App).networkManager
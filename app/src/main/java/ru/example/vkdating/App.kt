package ru.example.vkdating

import android.app.Application
import ru.example.vkdating.data.AuthPreferences
import ru.example.vkdating.data.UserPreferences
import ru.example.vkdating.data.network.NetworkManager
import timber.log.Timber

class App : Application() {

    lateinit var authPreference: AuthPreferences
        private set
    lateinit var userPreference: UserPreferences
        private set
    lateinit var networkManager: NetworkManager
        private set

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        authPreference = AuthPreferences(this)
        userPreference = UserPreferences(this)
        networkManager = NetworkManager()
    }

}
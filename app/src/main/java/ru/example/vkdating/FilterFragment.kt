package ru.example.vkdating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_filter.*
import ru.example.vkdating.core.userPreferences

class FilterFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_filter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewValues()
        setupViews()
    }

    private fun initViewValues() {
        cbOnline.isChecked = userPreferences.online
        etAgeFrom.setText(userPreferences.ageFrom?.toString())
        etAgeTo.setText(userPreferences.ageTo?.toString())
    }

    private fun setupViews() {
        btnSave.setOnClickListener {
            userPreferences.online = cbOnline.isChecked
            userPreferences.ageFrom = etAgeFrom.text.toString().toIntOrNull()
            userPreferences.ageTo = etAgeTo.text.toString().toIntOrNull()
            replaceScreen(Screens.ChooseCouple)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): FilterFragment = FilterFragment()
    }

}
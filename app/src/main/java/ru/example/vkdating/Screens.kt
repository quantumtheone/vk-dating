package ru.example.vkdating

import androidx.fragment.app.Fragment
import ru.example.vkdating.core.Screen

object Screens {

    object Auth : Screen() {
        override fun fragment(): Fragment = AuthFragment.newInstance()
    }

    object ChooseCouple : Screen() {
        override fun fragment(): Fragment = ChooseCoupleFragment.newInstance()
    }

    object Filter : Screen() {
        override fun fragment(): Fragment = FilterFragment.newInstance()
    }

}


package ru.example.vkdating

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_auth.*
import ru.example.vkdating.core.authPreferences
import timber.log.Timber

class AuthFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_auth, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (authPreferences.isAuth) goToChooseCouple() else loadAuthLink()
    }

    private fun loadAuthLink() {
        val redirectUrl = "https://oauth.vk.com/blank.html"
        val appClientId = 6984457
        val display = "mobile"
        val apiVersion = 5.101
        val scope = "friends"
        val authUrl = "https://oauth.vk.com/authorize?" +
                "client_id=$appClientId&" +
                "display=$display&" +
                "redirect_uri=$redirectUrl&" +
                "scope=$scope&" +
                "response_type=token&" +
                "v=$apiVersion&"
        wvAuth.webViewClient = object : WebViewClient() {
            @Suppress("DEPRECATION")
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return if (url.startsWith(redirectUrl, true)) {
                    handleAuthRedirect(url)
                    true
                } else {
                    super.shouldOverrideUrlLoading(view, url)
                }
            }

            @RequiresApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                val url = request.url.toString()
                return if (request.isRedirect && url.startsWith(redirectUrl, true)) {
                    handleAuthRedirect(url)
                    true
                } else {
                    super.shouldOverrideUrlLoading(view, request)
                }
            }
        }
        wvAuth.loadUrl(authUrl)
    }

    fun handleAuthRedirect(url: String) {
        val redirectParams = url.substringAfter("#")
            .split("&")
            .associate {
                val keyValue = it.split("=")
                val key = keyValue[0]
                val value = keyValue[1]
                key to value
            }
        val accessToken = redirectParams["access_token"]
        val userId = redirectParams["user_id"]
        Timber.d("Token: $accessToken userId: $userId")
        authPreferences.token = accessToken
        authPreferences.userId = userId?.toLongOrNull() ?: 0
        goToChooseCouple()
    }

    private fun goToChooseCouple() {
        requireFragmentManager().beginTransaction()
            .replace(R.id.vgContainer, Screens.ChooseCouple.fragment())
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance(): AuthFragment = AuthFragment()
    }

}
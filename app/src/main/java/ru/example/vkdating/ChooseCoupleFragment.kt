package ru.example.vkdating

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_choose_couple.*
import ru.example.vkdating.core.authPreferences
import ru.example.vkdating.core.networkManager
import ru.example.vkdating.core.plusAssign
import ru.example.vkdating.core.userPreferences
import ru.example.vkdating.data.network.api.VkApiParams.apiVersion
import ru.example.vkdating.data.network.api.users.Sex
import ru.example.vkdating.data.network.api.users.UserResponse
import ru.example.vkdating.data.network.api.users.UsersApi
import timber.log.Timber

class ChooseCoupleFragment : Fragment() {

    private val usersApi: UsersApi by lazy { networkManager.createApi<UsersApi>() }
    private val disposables: CompositeDisposable = CompositeDisposable()

    private val token: String get() = authPreferences.token ?: ""
    private val pageSize = 20
    private val getPageThreshold = pageSize / 2
    private var isGetUsersInProgress = false

    private var user: UserResponse? = null
    private var searchOffset = 0
    private var currentPosition = 0

    private var couples: MutableList<UserResponse> = mutableListOf()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_choose_couple, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        getSelfUser()
    }

    private fun setupViews() {
        btnNo.setOnClickListener { showNextCouple() }
        btnYes.setOnClickListener {
            // TODO: ET 2020-01-12 save user
            showNextCouple()
        }
        ivFilter.setOnClickListener { addScreen(Screens.Filter) }
    }

    private fun showNextCouple() {
        if (currentPosition < searchOffset)
            showUser(couples.getOrNull(++currentPosition))
        requestCouplesIfNeed()
        Timber.d("currentPosition: $currentPosition")
    }

    private fun requestCouplesIfNeed() {
        if (currentPosition > searchOffset - getPageThreshold) {
            if (isGetUsersInProgress.not()) {
                getUsers()
            }
        }
    }

    private fun getSelfUser() {
        disposables += usersApi.getUsers(apiVersion, token, listOf(authPreferences.userId))
                .map { it.response?.first() }
                .doOnSuccess { user = it }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ getUsers() }, ::showError)
    }

    private fun getUsers() {
        isGetUsersInProgress = true
        val requestOffset = searchOffset
        val getUsers = Single.defer {
            val sex = Sex.valueOf(user?.sex)
            val searchSex = when (sex) {
                Sex.FEMALE -> Sex.MALE
                Sex.MALE -> Sex.FEMALE
                else -> null
            }?.value
            usersApi.searchUsers(
                    apiVersion,
                    token,
                    requestOffset,
                    country = user?.country?.id,
                    city = user?.city?.id,
                    sex = searchSex,
                    ageFrom = userPreferences.ageFrom,
                    ageTo = userPreferences.ageTo,
                    online = if (userPreferences.online) 1 else 0
            )
        }
        disposables += getUsers
                .map { it.response?.items ?: listOf() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { searchOffset += it.size }
                .doAfterTerminate { isGetUsersInProgress = false }
                .subscribe({
                    if (requestOffset == 0)
                        showUser(it.firstOrNull())
                    couples.addAll(it)
                    Timber.d("offset: $requestOffset $it")
                }, ::showError)
    }

    private fun showUser(userResponse: UserResponse?) {
        tvFirstName.text = userResponse?.firstName
        showAvatar(userResponse?.photo200)
    }

    private fun showAvatar(url: String?) {
        Glide.with(this)
                .load(url)
                .into(ivAvatar)
    }

    private fun showError(throwable: Throwable) {
        Toast.makeText(requireContext(), "Не удалось загрузить данные", Toast.LENGTH_SHORT)
                .show()
        Timber.e(throwable)
    }

    override fun onDestroyView() {
        disposables.clear()
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(): ChooseCoupleFragment = ChooseCoupleFragment()
    }

}
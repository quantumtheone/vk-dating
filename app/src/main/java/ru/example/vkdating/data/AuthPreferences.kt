package ru.example.vkdating.data

import android.content.Context
import android.content.SharedPreferences

class AuthPreferences(context: Context) {

    private val sharedPreference: SharedPreferences =
            context.getSharedPreferences("Auth", Context.MODE_PRIVATE)

    var token: String? = null
        get() = sharedPreference.getString(AUTH_TOKEN, null)
        set(value) {
            sharedPreference.edit()
                    .putString(AUTH_TOKEN, value)
                    .apply()
            field = value
        }
    var userId: Long = 0
        get() = sharedPreference.getLong(USER_ID, 0)
        set(value) {
            sharedPreference.edit()
                    .putLong(USER_ID, value)
                    .apply()
            field = value
        }
    val isAuth: Boolean get() = token != null

    fun clear() {
        token = null
        userId = 0
    }

    private companion object {
        const val AUTH_TOKEN = "auth_token"
        const val USER_ID = "user_id"
    }

}
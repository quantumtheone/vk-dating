package ru.example.vkdating.data

import android.content.Context
import android.content.SharedPreferences

class UserPreferences(context: Context) {

    private val sharedPreference: SharedPreferences =
            context.getSharedPreferences("User", Context.MODE_PRIVATE)

    var ageFrom: Int? = null
        get() {
            val defaultValue = 0
            val value = sharedPreference.getInt(AGE_FROM, defaultValue)
            return if (value == defaultValue) null else return value
        }
        set(value) {
            sharedPreference.edit()
                    .putInt(AGE_FROM, value ?: 0)
                    .apply()
            field = value
        }

    var ageTo: Int? = null
        get() {
            val defaultValue = 0
            val value = sharedPreference.getInt(AGE_TO, defaultValue)
            return if (value == defaultValue) null else return value
        }
        set(value) {
            sharedPreference.edit()
                    .putInt(AGE_TO, value ?: 0)
                    .apply()
            field = value
        }
    var online: Boolean
        get() = sharedPreference.getBoolean(ONLINE, false)
        set(value) {
            sharedPreference.edit()
                    .putBoolean(ONLINE, value)
                    .apply()
        }

    fun clear() {
        ageFrom = 0
        ageTo = 0
    }

    private companion object {
        const val AGE_FROM = "age_from"
        const val AGE_TO = "age_to"
        const val ONLINE = "online"
    }

}
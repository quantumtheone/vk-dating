package ru.example.vkdating.data.network.api

import com.google.gson.annotations.SerializedName

class PageResponse<T>(
    @SerializedName("count")
    val count: Long,
    @SerializedName("items")
    val items: List<T>
)
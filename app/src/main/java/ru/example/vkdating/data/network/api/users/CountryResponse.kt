package ru.example.vkdating.data.network.api.users

import com.google.gson.annotations.SerializedName

class CountryResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String
)
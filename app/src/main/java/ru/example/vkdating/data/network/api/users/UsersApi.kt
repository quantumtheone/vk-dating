package ru.example.vkdating.data.network.api.users

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.example.vkdating.data.network.api.PageResponse
import ru.example.vkdating.data.network.api.Response

interface UsersApi {

    @GET("users.get")
    fun getUsers(
        @Query("v") apiVersion: String,
        @Query("access_token") token: String,
        @Query("ids") ids: List<Long>,
        @Query("fields") fields: List<String> = listOf("photo_200", "country", "city, sex")
    ): Single<Response<List<UserResponse>>>

    @GET("users.search")
    fun searchUsers(
        @Query("v") apiVersion: String,
        @Query("access_token") token: String,
        @Query("offset") offset: Int,
        @Query("country") country: Long? = null,
        @Query("city") city: Long? = null,
        @Query("sex") sex: Int? = null,
        @Query("fields") fields: List<String> = listOf("photo_200"),
        @Query("age_from") ageFrom: Int? = null,
        @Query("age_to") ageTo: Int? = null,
        @Query("online") online: Int? = null
    ): Single<Response<PageResponse<UserResponse>>>

}
package ru.example.vkdating.data.network.api.users

import com.google.gson.annotations.SerializedName

class UserResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("is_closed")
    val isClosed: Boolean,
    @SerializedName("photo_200")
    val photo200: String,
    @SerializedName("city")
    val city: CityResponse?,
    @SerializedName("country")
    val country: CityResponse?,
    @SerializedName("sex")
    val sex: Int?
)

enum class Sex(val value: Int) {
    ALL(0),
    FEMALE(1),
    MALE(2);

    companion object {
        @JvmStatic
        fun valueOf(value: Int?): Sex = when (value) {
            1 -> FEMALE
            2 -> MALE
            else -> ALL
        }
    }
}

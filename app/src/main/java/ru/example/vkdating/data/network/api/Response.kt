package ru.example.vkdating.data.network.api

import com.google.gson.annotations.SerializedName

class Response<T>(
    @SerializedName("response")
    val response: T?,
    @SerializedName("error")
    val error: ErrorResponse
)

class ErrorResponse(
    @SerializedName("error_code")
    val errorCode: Int,
    @SerializedName("error_msg")
    val errorMessage: String,
    @SerializedName("request_params")
    val requestParams: List<ErrorRequestParam>
)

class ErrorRequestParam(
    @SerializedName("key")
    val key: String,
    @SerializedName("value")
    val value: String
)
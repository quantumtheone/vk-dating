package ru.example.vkdating.data.network.api.users

import com.google.gson.annotations.SerializedName

class CityResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String
)